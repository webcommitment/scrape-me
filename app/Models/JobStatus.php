<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JobStatus extends Model
{
    use HasFactory;

    protected $table = 'job_status';
    public $timestamps = false;
    /**
     * Get the jobs for the status.
     */
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs');
    }
}
