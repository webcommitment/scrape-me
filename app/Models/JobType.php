<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class JobType extends Model
{
    use HasFactory;

    protected $table = 'job_type';
    public $timestamps = false;
    /**
     * Get the notes for the status.
     */
    public function jobs()
    {
        return $this->hasMany('App\Models\Jobs');
    }
}
