<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jobs extends Model
{

    use HasFactory;

    protected $table = 'jobs';

    /**
     * Get the User that owns the Jobs.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'users_id')->withTrashed();
    }

    /**
     * Get the Status that owns the Jobs.
     */
    public function job_status()
    {
        return $this->belongsTo('App\Models\JobStatus', 'status_id');
    }

    /**
     * Get the Job type that owns the Jobs.
     */
    public function job_type()
    {
        return $this->belongsTo('App\Models\JobType', 'type_id');
    }

    /**
     * Get the Frequency that owns the Jobs.
     */
    public function frequency()
    {
        return $this->belongsTo('App\Models\Frequency', 'frequency_id');
    }
}
