<?php

namespace App\Http\Controllers;

use App\Models\Frequency;
use App\Models\JobType;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\DB;
use App\Models\Jobs;
use App\Models\JobStatus;
use Illuminate\Support\Facades\Log;

class JobsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Jobs::with('user')->with('job_status')->with('job_type')->with('frequency')->paginate( 20 );
        return view('dashboard.jobs.jobsList', ['jobs' => $jobs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = JobStatus::all();
        $types = JobType::all();
        $frequencies = Frequency::all();
        return view('dashboard.jobs.create', [ 'job_statuses' => $statuses, 'job_types' => $types,'frequencies' => $frequencies ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::debug($request. 'dsasa data.');
        $validatedData = $request->validate([
            'title'             => 'required|min:1|max:64',
            'searchQuery'           => 'required',
            'frequency_id'         => 'required',
            'status_id'         => 'required',
            'type_id'         => 'required'
        ]);

        $user = auth()->user();
        $job = new Jobs();
        $job->title     = $request->input('title');
        $job->searchQuery   = $request->input('searchQuery');
        $job->frequency_id = $request->input('frequency_id');
        $job->status_id = $request->input('status_id');
        $job->type_id = $request->input('type_id');
        $job->users_id = $user->id;
        $job->save();

        $request->session()->flash('message', 'Successfully created scraper job');
        return redirect()->route('jobs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = Jobs::with('user')->with('job_status')->with('frequency')->with('job_type')->find($id);

        $path = '../linkedin-scraper/storage/scraped/'. $job->job_type->id .'/' . $job->id .'/scraped-posts.json';
        $data = json_decode(file_get_contents($path));
        return view('dashboard.jobs.jobsShow', [ 'job' => $job, 'data' => $data ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Jobs::find($id);
        $statuses = JobStatus::all();
        $frequencies = Frequency::all();
        $types = JobType::all();
        return view('dashboard.jobs.edit', [ 'job_statuses' => $statuses, 'job' => $job, 'job_types' => $types, 'frequencies' => $frequencies ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //var_dump('bazinga');
        //die();
        $validatedData = $request->validate([
            'title'             => 'required|min:1|max:64',
            'searchQuery'           => 'required',
            'frequency_id'         => 'required',
            'status_id'         => 'required',
            'type_id'         => 'required'
        ]);
        $job = Jobs::find($id);
        $job->title     = $request->input('title');
        $job->searchQuery   = $request->input('searchQuery');
        $job->frequency_id = $request->input('frequency_id');
        $job->status_id = $request->input('status_id');
        $job->type_id = $request->input('type_id');
        $job->save();
        $request->session()->flash('message', 'Successfully edited job');
        return redirect()->route('jobs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Jobs::find($id);
        if($job){
            $job->delete();
        }
        return redirect()->route('.index');
    }
}
