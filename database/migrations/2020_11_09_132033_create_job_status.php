<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('class');
        });

        // Insert job statuses
        DB::table('job_status')->insert(
            array(
                array(
                    'id' => 11,
                    'name' => 'Active',
                    'class' => 'badge badge-pill badge-primary'
                ),
                array(
                    'id' => 12,
                    'name' => 'Disabled',
                    'class' => 'badge badge-pill badge-warning'
                )

            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_status');
    }
}
