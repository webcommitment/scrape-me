<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrequencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });

        // Insert frequencies
        DB::table('frequency')->insert(
            array(
                array(
                    'id' => 21,
                    'name' => 'Every minute'
                ),
                array(
                    'id' => 22,
                    'name' => 'Every hour'
                ),
                array(
                    'id' => 23,
                    'name' => 'Every day'
                ),
                array(
                    'id' => 24,
                    'name' => 'Every week'
                ),
                array(
                    'id' => 25,
                    'name' => 'Every month'
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequency');
    }
}
