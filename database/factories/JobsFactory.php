<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Jobs;
use App\Models\User as User;
use App\Models\JobStatus;

class JobsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jobs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'         => $this->faker->sentence(4,true),
            'content'       => $this->faker->paragraph(3,true),
            'searchQuery'     => JobStatus::factory()->create()->id,
            'users_id'      => User::factory()->create()->id
        ];
    }
}
