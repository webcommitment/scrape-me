const pageScraper = require('./pageScraper');
const chalk = require("chalk");
const error = chalk.bold.red;
async function scrapeAll(browserInstance, job){
    let browser;
    try{
        browser = await browserInstance;
        job = await job;
        await pageScraper.scraper(browser, job);

    }
    catch(err){
        console.log(error("Could not resolve the browser instance => ", err));
    }
}

module.exports = (browserInstance, job) => scrapeAll(browserInstance, job)
