const puppeteer = require('puppeteer');
const chalk = require("chalk");

const error = chalk.bold.red;
const success = chalk.keyword("green");

async function startBrowser(){
    let browser;
    try {
        console.log("Opening the browser...");
        browser = await puppeteer.launch({
            headless: false,
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
            'ignoreHTTPSErrors': true
        });
    } catch (err) {
        console.log(error("Could not create a browser instance => : ", err));
    }
    return browser;
}

module.exports = {
    startBrowser
};
