var fs = require('fs');
const chalk = require("chalk");
require('dotenv').config();
const linkedinSignInPageUrl = process.env.LINKEDIN_SIGN_IN_PAGE_URL;
const linkedinUsername = process.env.LINKEDIN_USERNAME;
const linkedinPassword = process.env.LINKEDIN_PASSWORD;

const error = chalk.bold.red;
const success = chalk.keyword("green");

var startTime, endTime;

function startCount() {
    startTime = new Date();
};

function endCount() {
    endTime = new Date();
    var timeDiff = endTime - startTime; //in ms
    // strip the ms
    timeDiff /= 1000;

    // get seconds
    var seconds = Math.round(timeDiff);
    console.log(success("== Job finished in: " + seconds + " seconds ✓ =="));
}

const scraperObject = {
    signInFormUrl: linkedinSignInPageUrl,
    username: linkedinUsername,
    password: linkedinPassword,
    async scraper(browser, job){
        startCount();
        let page = await browser.newPage();
        await page.goto(this.signInFormUrl);
        console.log(`Navigating to ${this.signInFormUrl}...`);

        await page.type('#username', this.username);
        await page.type('#password', this.password);
        await page.click('button.btn__primary--large');
        await page.waitForNavigation();
        console.log(`== Successful login ✓ ==`);

        await page.goto(job[1]); // get search query
        console.log(`Navigating to ${job[1]}...`);

        await page.waitForSelector('.search-results-page');
        console.log(`== Search results loaded ✓ ==`);

        let posts = await page.evaluate(() => {
            var profileImageList = document.querySelectorAll('.entity-result__image');
            var profileTitleList = document.querySelectorAll('.entity-result__title-text a');
            var contentList = document.querySelectorAll('.entity-result__content-inner-container');
            var postLinkList = document.querySelectorAll('.entity-result__content-inner-container a');

            var postsArr = [];

            for (var i = 0; i < profileTitleList.length; i++) {

                postsArr[i] = {
                    profileImageSrc: profileImageList[i].innerHTML,
                    title: profileTitleList[i].innerText.trim(),
                    profileLink: profileTitleList[i].getAttribute('href'),
                    content: contentList[i].innerHTML,
                    postLink: postLinkList[i].getAttribute('href'),
                };
            }
            return postsArr;
        })

        await browser.close();
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "-"
            + (currentdate.getMonth()+1)  + "-"
            + currentdate.getFullYear() + "_"
            + currentdate.getHours() + "-"
            + currentdate.getMinutes() + "-"
            + currentdate.getSeconds();

        var fileName = 'scraped-posts.json';
        var dir = './storage/scraped/' + job[3] + '/' + job[5]; // job[3] = job_type , job[5] = job id


        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir, { recursive: true });
            try {
                if (! fs.existsSync(dir + '/' + fileName)) {
                    fs.writeFile(dir + '/' + fileName , JSON.stringify(posts), function(err) {
                        if (err) throw err;
                        console.log(success(`== Scraped data changed, File successfully saved ✓ ==`));
                    });
                }
            } catch(err) {
                console.error(err)
            }
        }

        let rawdata = fs.readFileSync(dir + '/' + fileName);
        let existingPosts = JSON.parse(rawdata);

        if (JSON.stringify(existingPosts) == JSON.stringify(posts)) {
            console.log(`== Scraped data hasn't change ==`);
        } else {
            fs.writeFile(dir + '/' + fileName , JSON.stringify(posts), function(err) {
                if (err) throw err;
                console.log(success(`== Scraped data changed, File successfully saved ✓ ==`));
            });
        }
        endCount();
    }
}

module.exports = scraperObject;
