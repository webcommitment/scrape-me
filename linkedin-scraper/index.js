const browserObject = require('./browser');
const scraperController = require('./pageController');
const cron = require('node-cron');
const express = require('express');
const axios = require('axios');
const https = require('https');
var fs = require('fs');
const chalk = require("chalk");

const successMsg = chalk.keyword("green");
const errorMsg = chalk.bold.red;
require('dotenv').config();

const appJobsUrl = process.env.APP_JOBS_ENDPOINT;

const agent = new https.Agent({
    rejectUnauthorized: false // TODO Change before go live, only for localhost dev purposes
});

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
console.log(successMsg('== Scraper app activated =='));

cron.schedule('* * * * *', function() {
axios.get(appJobsUrl, {httpsAgent: agent})
    .then(({ data }) => {
        // map a new array
        let jobs = data.map(
            job => [
                job.title,
                job.searchQuery,
                job.frequency_id,
                job.type_id,
                job.status_id,
                job.id
            ]
        );

        let filteredJobs = jobs.filter(function( obj ) {
            return obj[4] !== 12;
        });

        // check if jobs file has changed
        let rawdata = fs.readFileSync('./storage/jobs/jobs.json');
        let existingJobs = JSON.parse(rawdata);
        if (JSON.stringify(existingJobs) == JSON.stringify(filteredJobs)) {
            console.log(`== Jobs data hasn't change ==`);
        } else {
            var currentdate = new Date();
            // if not, write a new file
            fs.writeFile(`./storage/jobs/` + 'jobs.json', JSON.stringify(filteredJobs), function (err) {
                if (err) throw err;
                console.log(successMsg(`== Jobs File successfully saved ✓ == ` + currentdate));
            });
        }
    })
    .catch(error=>{
        console.log(errorMsg(error));
    });
});

// Cron job settings
app = express();

const everyMinute = '* * * * *';
const everyHour = '0 * * * *';
const everyDay = '0 0 0 * * *';
const everyWeek = '0 0 * * 0';
const everyMonth = '0 0 0 * *';

function getFrequency(val) {
    let frequency = "";
    switch( val ) {
        case 21:
            frequency = everyMinute;
            break;
        case 22:
            frequency = everyHour;
            break;
        case 23:
            frequency = everyDay;
            break;
        case 24:
            frequency = everyWeek;
            break;
        case 25:
            frequency = everyMonth;
            break;
        default:
            frequency = everyMinute;
    }
    return frequency;
}

let rawJobs = fs.readFileSync('./storage/jobs/jobs.json');
let allJobs = JSON.parse(rawJobs);

allJobs.forEach(job => {
    let cronFrquency = getFrequency(job[2]);
    cron.schedule(cronFrquency, function() {
        console.log(`== Running the ` + job[0] + ` job  ✓ == `);
         let browserInstance = browserObject.startBrowser();
         scraperController(browserInstance, job)
    });
})
app.listen(3000);
