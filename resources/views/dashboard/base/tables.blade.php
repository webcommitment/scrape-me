@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <!-- /.row-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><i class="fa fa-align-justify"></i>Scraped LinkedIn Posts</div>
                        <div class="card-body">
                            <table class="table table-responsive-sm table-bordered table-striped table-sm">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Profilename</th>
                                    <th>Function</th>
                                    <th>Post content</th>
                                    <th>Time posted</th>
                                    <th>Post link</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td>Vishnu Serghei</td>
                                    <td>CEO, Philips</td>
                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id porttitor
                                        velit. Nunc mollis ligula id mi sollicitudin, eu ultrices velit dictum. Praesent
                                        eget tellus at leo pellentesque laoreet mattis nec augue. Donec efficitur nisl
                                        massa. Fusce eget sapien mollis eros aliquet blandit. Nam volutpat ullamcorper
                                        justo vitae varius. Praesent tincidunt massa at purus feugiat, vitae tristique
                                        urna tristique. Nullam et felis vitae lacus sodales luctus.
                                    </td>
                                    <td>2h</td>
                                    <td><a href="#">Read more</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>

@endsection

@section('javascript')

@endsection
