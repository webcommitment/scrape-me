@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-9 col-xl-10">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> Job title: {{ $job->title }}
                        </div>
                        @include('dashboard/jobs/data-scraping-table')
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3 col-xl-2">
                    <div class="card">
                        <div class="card-body">
                            <h4>Author:</h4>
                            <p> {{ $job->user->name }}</p>
                            <h4>Title:</h4>
                            <p> {{ $job->title }}</p>
                            <h4>Search query:</h4>
                            <p>{{ $job->searchQuery }}</p>
                            <h4>Frequency</h4>
                            <p>{{ $job->frequency->name }}</p>
                            <h4> Status: </h4>
                            <p>
                                          <span class="{{ $job->job_status->class }}">
                                              {{ $job->job_status->name }}
                                          </span>
                            </p>
                            <h4>Job type:</h4>
                            <p>{{ $job->job_type->name }}</p>
                            <a href="{{ route('jobs.index') }}" class="btn btn-block btn-secondary">{{ __('Back') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('javascript')

@endsection
