@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>{{ __('Jobs') }}</div>
                        <div class="card-body">
                            <div class="row">
                                <a href="{{ route('jobs.create') }}" class="btn btn-primary m-2">{{ __('Add Job') }}</a>
                            </div>
                            <br>
                            <table class="table table-responsive-sm table-striped">
                                <thead>
                                <tr>
                                    <th>Author</th>
                                    <th>Title</th>
                                    <th>Search query</th>
                                    <th>Frequency</th>
                                    <th>Status</th>
                                    <th>Job type</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($jobs as $job)
                                    <tr>
                                        <td><strong>{{ $job->user->name }}</strong></td>
                                        <td><strong>{{ $job->title }}</strong></td>
                                        <td>{{ $job->searchQuery }}</td>
                                        <td>
                                              <span class="{{ $job->frequency->class }}">
                                                  {{ $job->frequency->name }}
                                              </span>
                                        </td>
                                        <td>
                                          <span class="{{ $job->job_status->class }}">
                                              {{ $job->job_status->name }}
                                          </span>
                                        </td>
                                        <td>
                                              <span class="{{ $job->job_type->class }}">
                                                  {{ $job->job_type->name }}
                                              </span>
                                        </td>
                                        <td>
                                            <a href="{{ url('/jobs/' . $job->id) }}" class="btn btn-block btn-primary">View</a>
                                        </td>
                                        <td>
                                            <a href="{{ url('/jobs/' . $job->id . '/edit') }}"
                                               class="btn btn-block btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('jobs.destroy', $job->id ) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-block btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $jobs->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('javascript')

@endsection

