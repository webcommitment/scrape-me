@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> {{ __('Edit') }}: {{ $job->title }}</div>
                        <div class="card-body">
                            <form method="POST" action="/notes/{{ $job->id }}">
                                @csrf
                                @method('PUT')
                                <div class="form-group row">
                                    <div class="col">
                                        <label>Title</label>
                                        <input class="form-control" type="text" placeholder="{{ __('Title') }}"
                                               name="title" value="{{ $job->title }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Search query</label>
                                        <textarea class="form-control" id="textarea-input" name="searchQuery" rows="9"
                                                  placeholder="{{ __('Search query..') }}"
                                                  required>{{ $job->searchQuery }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Frequency</label>
                                        <select class="form-control" name="frequency_id">
                                            @foreach($frequencies as $frequency)
                                                @if( $frequency->id == $job->frequency_id )
                                                    <option value="{{ $frequency->id }}"
                                                            selected="true">{{ $frequency->name }}</option>
                                                @else
                                                    <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Job status</label>
                                        <select class="form-control" name="status_id">
                                            @foreach($job_statuses as $status)
                                                @if( $status->id == $job->status_id )
                                                    <option value="{{ $status->id }}"
                                                            selected="true">{{ $status->name }}</option>
                                                @else
                                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Scraper job type</label>
                                        <select class="form-control" name="job_type">
                                            @foreach($job_types as $type)
                                                @if( $type->id == $job->type_id )
                                                    <option value="{{ $type->id }}"
                                                            selected="true">{{ $type->name }}</option>
                                                @else
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <button class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                                <a href="{{ route('jobs.index') }}"
                                   class="btn btn-block btn-secondary">{{ __('Back') }}</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
