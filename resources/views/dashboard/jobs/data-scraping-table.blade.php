<div class="card-body">
    <table class="table table-responsive-sm table-bordered table-striped table-sm">
        <thead>
        <tr>
            <th></th>
            <th>Profilename</th>
            <th>Function</th>
            <th>Post content</th>
            <th>Time posted</th>
            <th>Post link</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key => $value)
            <tr>
                <td>{!! $value->profileImageSrc !!}</td>
                <td>
                    <a href="{!! $value->profileLink !!}"
                       target="_blank"
                    >
                        {!! $value->title !!}
                    </a>
                </td>
                <td>==</td>
                <td>{!! $value->content !!}</td>
                <td>==</td>
                <td>
                    <a href="{!! $value->postLink !!}"
                       target="_blank"
                    >Read more
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
