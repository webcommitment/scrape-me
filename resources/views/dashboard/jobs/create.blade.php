@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i> {{ __('Create Job') }}</div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('jobs.store') }}">
                                @csrf
                                <div class="form-group row">
                                    <div class="col">
                                        <label>Title</label>
                                        <input class="form-control" type="text" placeholder="{{ __('Title') }}"
                                               name="title" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Search query</label>
                                        <textarea class="form-control" id="textarea-input" name="searchQuery" rows="9"
                                                  placeholder="{{ __('Search query..') }}" required></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Frequency</label>
                                        <select class="form-control" name="frequency_id">
                                            @foreach($frequencies as $frequency)
                                                <option value="{{ $frequency->id }}">{{ $frequency->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Status</label>
                                        <select class="form-control" name="status_id">
                                            @foreach($job_statuses as $status)
                                                <option value="{{ $status->id }}">{{ $status->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <label>Scraper job type</label>
                                        <select class="form-control" name="type_id">
                                            @foreach($job_types as $job_type)
                                                <option value="{{ $job_type->id }}">{{ $job_type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <button class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                                <a href="{{ route('jobs.index') }}"
                                   class="btn btn-block btn-secondary">{{ __('Back') }}</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
